#!/bin/bash

echo '' > .env

echo "# Configuration utilisateur" >> .env
echo "USER_ID=$(id -u $USER)" >> .env
echo "GROUP_ID=$(id -g $USER)" >> .env
echo "" >> .env

echo "The .env file has been successfully generated"