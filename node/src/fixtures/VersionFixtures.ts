import Version from "@/entities/Repository/Version";
import RepositoryFixtures from "@/fixtures/RepositoryFixtures";
import PatchNoteSection from "@/entities/MergeRequest/PatchNoteSection";

const VersionFixtures: Array<Version> = [
    new Version('1', '1.1.2', RepositoryFixtures[0],
        [
            new PatchNoteSection('features', 'Fonctionnalités', ['Line 1', 'Line 2'], true),
            new PatchNoteSection('fix', 'Correctifs', ['Description'], true),
            new PatchNoteSection('refacto', 'Refacto', ['Line 1', 'Line 2', 'Line 3'], true),
            new PatchNoteSection('todo', 'TODO', ['Description'], true),
            new PatchNoteSection('other', 'Autre', ['Description'], true),
            new PatchNoteSection('test', 'eazeaeza', ['Description 2'], true),
        ]
    ),
    new Version('2', '1.1.3', RepositoryFixtures[0]),
    new Version('3', '2.0.3', RepositoryFixtures[1],
        [
            new PatchNoteSection('features', 'Fonctionnalités', ['Description'], true),
            new PatchNoteSection('fix', 'Correctifs', ['Description'], true),
            new PatchNoteSection('refacto', 'Refacto', ['Description'], true),
            new PatchNoteSection('todo', 'TODO', ['Description'], true),
            new PatchNoteSection('internal', 'Interne', ['Description'], true),
            new PatchNoteSection('other', 'Autre', ['Description'], true),
        ]
    ),
    new Version('4', '6.5.2', RepositoryFixtures[2],
        [
            new PatchNoteSection('features', 'Fonctionnalités', ['Description'], true),
            new PatchNoteSection('fix', 'Correctifs', ['Description'], true),
            new PatchNoteSection('refacto', 'Refacto', ['Description'], true),
            new PatchNoteSection('todo', 'TODO', ['Description'], true),
            new PatchNoteSection('internal', 'Interne', ['Description'], true),
            new PatchNoteSection('other', 'Autre', ['Description'], true),
        ]
    ),
    new Version('5', '0.0.1', RepositoryFixtures[3]),
    new Version('6', '5.5.5', RepositoryFixtures[4],
        [
            new PatchNoteSection('features', 'Fonctionnalités', ['Description'], true),
            new PatchNoteSection('fix', 'Correctifs', ['Description'], true),
            new PatchNoteSection('refacto', 'Refacto', ['Description'], true),
        ]
    ),
];

export default VersionFixtures;