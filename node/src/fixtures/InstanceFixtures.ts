import RepositoryFixtures from "@/fixtures/RepositoryFixtures";
import Instance from "@/entities/Instance/Instance";
import VersionFixtures from "@/fixtures/VersionFixtures";
import VersionHistory from "@/entities/Instance/VersionHistory";

const InstanceFixtures: Array<Instance> = [
    new Instance('1', 'Instance', RepositoryFixtures[0], null, [
        new VersionHistory(2, VersionFixtures[1], new Date()),
        new VersionHistory(1, VersionFixtures[0], new Date()),
    ]),
    new Instance('2', 'Instance principale', RepositoryFixtures[0], null, [
        new VersionHistory(3, VersionFixtures[1], new Date()),
    ]),
    new Instance('3', 'Instance avec un grand titre', RepositoryFixtures[1], null, [
        new VersionHistory(3, VersionFixtures[2], new Date()),
    ]),
    new Instance('4', 'Instance avec un très grand titre', RepositoryFixtures[2], null, [
        new VersionHistory(3, VersionFixtures[3], new Date()),
    ]),
    new Instance('5', 'Instance avec un très très grand titre', RepositoryFixtures[3], null, [
        new VersionHistory(3, VersionFixtures[4], new Date()),
    ]),
    new Instance('6', 'Instance avec un nom de barbare grecque en PLS', RepositoryFixtures[4], null, [
        new VersionHistory(3, VersionFixtures[5], new Date()),
    ]),
];

export default InstanceFixtures;
