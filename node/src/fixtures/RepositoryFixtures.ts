import Repository from "@/entities/Repository/Repository";

const RepositoryFixtures: Array<Repository> = [
    new Repository('a3088719-1e62-4623-8af2-e742bfc98a24', 'Repo', 'https://link.repo.localhost/1'),
    new Repository('fc498c79-d204-44e7-8af3-fc1b3bff19e0', 'Repository', 'https://link.repo.localhost/2'),
    new Repository('695dd0c5-8659-4f2a-ad26-9d6172736b04', 'Repo avec un grand titre', 'https://link.repo.localhost/3'),
    new Repository('9bcaf697-97da-4893-ac91-a277006d036b', 'Repo Repo avec un très grand titre', 'https://link.repo.localhost/4'),
    new Repository('ce8de089-f7c2-4a60-a1f1-ff791b4e2e2f', 'Repository avec un nom de barbare grecque en PLS', 'https://link.repo.localhost/5'),
];

RepositoryFixtures[0].addSubModule(RepositoryFixtures[1]);
RepositoryFixtures[0].addSubModule(RepositoryFixtures[2]);
RepositoryFixtures[0].addSubModule(RepositoryFixtures[3]);
RepositoryFixtures[4].addSubModule(RepositoryFixtures[0]);

export default RepositoryFixtures;
