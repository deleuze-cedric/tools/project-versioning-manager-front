import MergeRequest from "@/entities/MergeRequest/MergeRequest";
import PatchNoteSection from "@/entities/MergeRequest/PatchNoteSection";
import RepositoryFixtures from "@/fixtures/RepositoryFixtures";
import VersionFixtures from "@/fixtures/VersionFixtures";

const MergeRequestFixtures: Array<MergeRequest> = [
    new MergeRequest(
        '1',
        "MR 1",
        'https://link.mr.localhost/1',
        RepositoryFixtures[0],
        [
            new PatchNoteSection('features', 'Fonctionnalités', ['Line 1', 'Line 2'], true),
            new PatchNoteSection('fix', 'Correctifs', ['Description'], true),
            new PatchNoteSection('refacto', 'Refacto', ['Line 1', 'Line 2', 'Line 3'], true),
            new PatchNoteSection('todo', 'TODO', ['Description'], true),
            new PatchNoteSection('internal', 'Interne', [], true),
            new PatchNoteSection('other', 'Autre', ['Description'], true),
            new PatchNoteSection('test', 'eazeaeza', ['Description 2'], true),
        ],
    ),
    new MergeRequest(
        '2',
        "Merge request principale",
        'https://link.mr.localhost/2',
        RepositoryFixtures[1],
        [
            new PatchNoteSection('features', 'Fonctionnalités', [], true),
            new PatchNoteSection('fix', 'Correctifs', [], true),
            new PatchNoteSection('refacto', 'Refacto', [], true),
            new PatchNoteSection('todo', 'TODO', [], true),
            new PatchNoteSection('internal', 'Interne', [], true),
            new PatchNoteSection('other', 'Autre', [], true),
        ],
        [],
        [],
        new Date(),
    ),
    new MergeRequest(
        '3',
        "Merge request avec un grand titre",
        'https://link.mr.localhost/3',
        RepositoryFixtures[2],
        [
            new PatchNoteSection('features', 'Fonctionnalités', ['Description'], true),
            new PatchNoteSection('fix', 'Correctifs', ['Description'], true),
            new PatchNoteSection('refacto', 'Refacto', ['Description'], true),
            new PatchNoteSection('todo', 'TODO', ['Description'], true),
            new PatchNoteSection('internal', 'Interne', ['Description'], true),
            new PatchNoteSection('other', 'Autre', ['Description'], true),
        ],
        [],
        [],
        null,
        new Date(),
    ),
    new MergeRequest(
        '4',
        "Merge request avec un très grand titre",
        'https://link.mr.localhost/4',
        RepositoryFixtures[3],
        [
            new PatchNoteSection('features', 'Fonctionnalités', ['Description'], true),
            new PatchNoteSection('fix', 'Correctifs', ['Description'], true),
            new PatchNoteSection('refacto', 'Refacto', ['Description'], true),
            new PatchNoteSection('todo', 'TODO', ['Description'], true),
            new PatchNoteSection('internal', 'Interne', ['Description'], true),
            new PatchNoteSection('other', 'Autre', ['Description'], true),
        ],
        [],
        [],
        new Date(),
        new Date(),
    ),
    new MergeRequest(
        '5',
        "Merge request avec un très très grand titre",
        'https://link.mr.localhost/5',
        RepositoryFixtures[4],
        []
    ),
    new MergeRequest(
        '6',
        "Merge request avec un nom de barbare grecque en PLS",
        'https://link.mr.localhost/6',
        RepositoryFixtures[0],
        [
            new PatchNoteSection('features', 'Fonctionnalités', ['Description'], true),
            new PatchNoteSection('fix', 'Correctifs', ['Description'], true),
            new PatchNoteSection('refacto', 'Refacto', ['Description'], true),
        ],
        [],
        [],
        null,
        new Date()
    )
];

MergeRequestFixtures[0].mergeRequestDependencies.push(MergeRequestFixtures[1]);
MergeRequestFixtures[0].versionDependencies.push(VersionFixtures[0]);

export default MergeRequestFixtures;
