import './assets/main.css'
import '../node_modules/flowbite-vue/dist/index.css';
import Wind from '@/presets/wind';

import {createApp} from 'vue'
import PrimeVue from 'primevue/config';
import {createPinia} from 'pinia'

import App from './App.vue'
import router from './router'
import ToastService from 'primevue/toastservice';

const app = createApp(App)
app.use(ToastService);

app.use(createPinia())
app.use(router)
app.use(PrimeVue, {
    unstyled: true,
    pt: Wind
} as any);

app.mount('#app')
