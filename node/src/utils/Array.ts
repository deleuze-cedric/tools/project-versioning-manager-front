export const swapElements = (array: Array<any>, index1: Number, index2: Number) => {
    if(typeof array[index1] === 'undefined') {
        return;
    }

    if(typeof array[index2]  === 'undefined') {
        return
    }
    let temp = array[index1];
    array[index1] = array[index2];
    array[index2] = temp;
};