import {createRouter, createWebHistory} from 'vue-router'
import RepositoryListView from "@/views/Repository/RepositoryListView.vue";
import MergeRequestList from "@/views/MergeRequest/MergeRequestList.vue";
import InstanceListView from "@/views/Instance/InstanceListView.vue";
import NotFoundView from "@/views/errors/NotFoundView.vue";
import RepositoryShowView from "@/views/Repository/RepositoryShowView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: RepositoryListView
    },
    {
      path: '/repositories',
      name: 'repository-list',
      component: RepositoryListView
    },
    {
      path: '/repository/:code',
      name: 'repository-show',
      component: RepositoryShowView
    },
    {
      path: '/merge-requests',
      name: 'mr-pr-list',
      component: MergeRequestList
    },
    {
      path: '/instances',
      name: 'instance-list',
      component: InstanceListView
    },
    {
      path: '/404',
      component: NotFoundView
    },
    {
      path: '/:catchAll(.*)',
      redirect: '/404'
    },
  ]
})

export default router
