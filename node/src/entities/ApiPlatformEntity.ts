export abstract class ApiPlatformEntity {

    protected constructor(protected _uri: string | null) {
    }

    public abstract toJson(): object;

    public abstract getIdentifier(): any;

    get uri(): string | null {
        return this._uri;
    }

    set uri(value: string | null) {
        this._uri = value;
    }
}