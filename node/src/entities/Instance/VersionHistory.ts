import Version from "@/entities/Repository/Version";
import {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";

export default class VersionHistory extends ApiPlatformEntity {

    constructor(
        private _id: Number,
        private _version: Version,
        private _versioned_at: Date,
        uri: string|null = null,
    ) {
        super(uri);
    }

    get id(): Number {
        return this._id;
    }

    get version(): Version {
        return this._version;
    }

    set version(value: Version) {
        this._version = value;

        return this;
    }

    get versioned_at(): Date {
        return this._versioned_at;
    }

    set versioned_at(value: Date) {
        this._versioned_at = value;

        return this;
    }

    toJson(): object {
        return {
            id: this.id,
            version: this.version,
            versioned_at: this.versioned_at.getUTCDate(),
        };
    }

    getIdentifier(): any {
        return this.id;
    }
}