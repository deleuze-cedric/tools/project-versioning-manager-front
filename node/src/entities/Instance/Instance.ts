import Repository from "@/entities/Repository/Repository";
import {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";
import VersionHistory from "@/entities/Instance/VersionHistory";

export default class Instance extends ApiPlatformEntity{

    constructor(
        private _uid?: String,
        private _name?: String,
        private _repository: Repository | null = null,
        private _img: null | String = null,
        private _history: Array<VersionHistory> = [],
        uri: string|null = null
    ) {
        super(uri);
    }

    get uid(): String {
        return this._uid ?? '';
    }

    get repository(): Repository | null {
        return this._repository;
    }

    set repository(value: Repository) {
        this._repository = value;

        return this;
    }

    get name(): String {
        return this._name ?? '';
    }

    set name(value: String) {
        this._name = value;
    }

    get history(): Array<VersionHistory> {
        return this._history;
    }

    public addHistory(history: VersionHistory) {
        this._history.push(history);

        return this;
    }

    public removeHistoryAtIndex(index: Number) {
        this._history.splice(index, 1);

        return this;
    }

    get img(): String {
        return this._img ?? 'https://seeklogo.com/images/V/vuejs-logo-17D586B587-seeklogo.com.png';
    }

    set img(img: String) {
        this._img = img;

        return this;
    }

    toJson(): object {
        return {
            uid: this.uid,
            name: this.name,
            repository: this.repository?.uri,
            img: this.img,
        };
    }

    getIdentifier(): any {
        return this.uid;
    }
}
