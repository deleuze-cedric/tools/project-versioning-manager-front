export interface ApiPlatformViolationInterface {
    propertyPath: String,
    message: String,
    code: String
}