import type {SHORTCUTS} from "@/constants/SHORTCUTS_PER_SECTION";

export default class Shortcut {

    constructor(
        public code: SHORTCUTS,
        public description: String,
        public combinaison: Array<String> = [],
        public alt: Boolean,
        public shift: Boolean,
        public ctrl: Boolean,
        public repeat: Boolean,
        public keyCode: Array<String>,
    ) {
    }

    public toString(): String {
        return this.combinaison.join('+');
    }

    public isValidEvent(event: KeyboardEvent): Boolean {
        return event.composed === this.combinaison?.length > 1 &&
            event.altKey === this.alt &&
            event.ctrlKey === this.ctrl &&
            event.shiftKey === this.shift &&
            this.keyCode.includes(event.code) &&
            (this.repeat || !event.repeat);
    }
}
