import type Shortcut from "@/entities/Shortcut/Shortcut";

export default class ShortcutSection {

    constructor(
        public name: String,
        public shortcuts: Array<Shortcut> = []
    ) {
    }

}