import {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";

export class BasedCode extends ApiPlatformEntity {
    constructor(
        public _id?: String,
        public _name?: String,
        uri: string|null = null,
    ) {
        super(uri);
    }

    get id(): String {
        return this._id ?? '';
    }

    get name(): String {
        return this._name ?? '';
    }

    toJson(): object {
        return {
            id: this.id,
            name: this.name,
        };
    }

    getIdentifier(): any {
        return this.id;
    }
}