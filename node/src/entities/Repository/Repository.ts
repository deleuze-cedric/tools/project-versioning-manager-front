import Version from "@/entities/Repository/Version";
import {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";
import MergeRequest from "@/entities/MergeRequest/MergeRequest";
import type {Submodule} from "@/entities/Repository/Submodule";

export default class Repository extends ApiPlatformEntity {

    constructor(
        private _code?: String,
        private _name?: String,
        private _link?: String,
        private _mainBranch?: String,
        private _devBranch?: String,
        private _basedCodeIdentifier?: String,
        private _token?: String,
        private _host?: String,
        private _createBranchForNewVersion: boolean = true,
        private _isAlreadyImported: boolean = false,
        private _img: null | String = null,
        private _versions: Array<Version> = [],
        private _mergeRequests: Array<MergeRequest> = [],
        private _subModules: Array<Submodule> = [],
        private _parentRepositories: Array<Submodule> = [],
        private _latestVersion: null | Version = null,
        private _latestVersionCode: null | String = null,
        uri: String | null = null,
    ) {
        super(uri);
    }

    get code(): String {
        return this._code ?? '';
    }

    set code(code: String) {
        this._code = code;

        return this;
    }

    get link(): String {
        return this._link ?? '';
    }

    set link(value: String) {
        this._link = value;

        return this;
    }

    get name(): String {
        return this._name ?? '';
    }

    set name(value: String) {
        this._name = value;

        return this;
    }

    get versions(): Array<Version> {
        return this._versions;
    }

    set versions(versions: Array<Version>) {
        this._versions = versions;

        return this;
    }

    public static fromJSON(d: Object): Repository {
        return Object.assign(new Repository(), d);
    }

    get img(): String {
        return this._img ?? 'https://seeklogo.com/images/V/vuejs-logo-17D586B587-seeklogo.com.png';
    }

    set img(img: String) {
        this._img = img;

        return this;
    }

    get mainBranch(): String {
        return this._mainBranch ?? '';
    }

    set mainBranch(value: String) {
        this._mainBranch = value;

        return this;
    }

    get devBranch(): String {
        return this._devBranch ?? '';
    }

    set devBranch(value: String) {
        this._devBranch = value;

        return this;
    }

    get mergeRequests(): Array<MergeRequest> {
        return this._mergeRequests;
    }

    set mergeRequests(mergeRequests: Array<MergeRequest>) {
        this._mergeRequests = mergeRequests;

        return this;
    }

    get subModules(): Array<Submodule> {
        return this._subModules;
    }

    set subModules(subModules: Array<Submodule>) {
        this._subModules = subModules;

        return this;
    }

    public addSubModule(module: Submodule) {
        this._subModules.push(module);
    }

    get parentRepositories(): Array<Submodule> {
        return this._parentRepositories;
    }

    set parentRepositories(parentRepositories: Array<Submodule>) {
        this._parentRepositories = parentRepositories;

        return this;
    }

    get isAlreadyImported(): boolean {
        return this._isAlreadyImported;
    }

    set isAlreadyImported(value: boolean) {
        this._isAlreadyImported = value;

        return this;
    }

    get createBranchForNewVersion(): boolean {
        return this._createBranchForNewVersion;
    }

    set createBranchForNewVersion(value: boolean) {
        this._createBranchForNewVersion = value;

        return this;
    }

    get basedCodeIdentifier(): String {
        return this._basedCodeIdentifier ?? '';
    }

    set basedCodeIdentifier(value: String) {
        this._basedCodeIdentifier = value;
    }

    get token(): String {
        return this._token ?? '';
    }

    set token(value: String) {
        this._token = value;
    }

    get host(): String {
        return this._host ?? '';
    }

    set host(value: String) {
        this._host = value;
    }

    toJson(): object {
        return {
            code: this.code,
            name: this.name,
            link: this.link,
            img: this.img,
            mainBranch: this.mainBranch,
            devBranch: this.devBranch,
            token: this.token,
            host: this.host,
            isAlreadyImported: this.isAlreadyImported,
            createBranchForNewVersion: this.createBranchForNewVersion,
            basedCodeIdentifier: this.basedCodeIdentifier,
            uri: this.uri,
            parentRepositories: this.parentRepositories.map((parentRepository: Submodule) => parentRepository.toJson()),
            subModules: this.subModules.map((subModule: Submodule) => subModule.toJson()),
        };
    }

    get latestVersion(): Version | null {
        return this._latestVersion;
    }

    get latestVersionFormattedCode(): String {
        return this._latestVersionCode ?? 'N.D.';
    }


    get latestVersionCode(): String | null {
        return this._latestVersionCode;
    }

    set latestVersionCode(code: string | null) {
        this._latestVersionCode = code;

        return this;
    }

    set latestVersion(value: Version | null) {
        this._latestVersion = value;
    }

    getIdentifier(): any {
        return this.code;
    }
}
