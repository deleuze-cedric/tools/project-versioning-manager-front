import type Repository from "@/entities/Repository/Repository";
import type MergeRequestDependencyInterface from "@/entities/MergeRequest/MergeRequestDependencyInterface";
import type MergeRequest from "@/entities/MergeRequest/MergeRequest";
import type PatchNoteSection from "@/entities/MergeRequest/PatchNoteSection";
import {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";

export default class Version extends ApiPlatformEntity implements MergeRequestDependencyInterface {

    constructor(
        private _uuid?: String,
        private _code?: String,
        private _repository: Repository | null = null,
        private _content: Array<PatchNoteSection> = [],
        private _merge_requests: Array<MergeRequest> = [],
        private _commits: Array<any> = [],
        uri: string | null = null,
    ) {
        super(uri);
        this.addVersionToRepository();
    }

    get repository(): Repository | null {
        return this._repository;
    }

    set repository(value: Repository) {
        this._repository = value;
        this.addVersionToRepository();
    }

    private addVersionToRepository() {
        if (!this._repository?.versions.includes(this)) {
            this._repository?.versions.push(this);
        }
    }

    get code(): String {
        return this._code ?? '';
    }

    set code(value: String) {
        this._code = value;

        return this;
    }

    get identifier(): any {
        return this.code;
    }

    public toString(): String {
        return this.repository?.name + '#' + this.code;
    }

    get name(): String {
        return this.code;
    }

    get link(): String {
        return this.repository?.link ?? '';
    }

    get uuid(): String {
        return this._uuid ?? '';
    }

    set uuid(value: String) {
        this._uuid = value;
    }

    get content(): Array<PatchNoteSection> {
        return this._content;
    }

    set content(value: Array<PatchNoteSection>) {
        this._content = value;
    }

    get merge_requests(): Array<MergeRequest> {
        return this._merge_requests;
    }

    set merge_requests(value: Array<MergeRequest>) {
        this._merge_requests = value;
    }

    get commits(): Array<any> {
        return this._commits;
    }

    set commits(value: Array<any>) {
        this._commits = value;
    }

    toJson(): object {
        return {
            uuid: this.uuid,
            code: this.code,
            repository: this.repository?.uri,
            content: this.content.map((content: PatchNoteSection) => content.toJson()),
            commits: this.commits,
        };
    }

    getIdentifier(): any {
        return this.uuid;
    }
}