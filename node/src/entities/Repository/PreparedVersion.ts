import type Repository from "@/entities/Repository/Repository";
import type Version from "@/entities/Repository/Version";
import type {VersionType} from "@/entities/Repository/VersionType";
import type {UpdateType} from "@/entities/Repository/UpdateType";

export interface PreparedVersion {
    repository: Repository,
    submoduleReleases: Array<PreparedVersion>,
    updateType: UpdateType,
    // New version
    versionType?: VersionType,
    versionCode?: String,
    releaseVersion?: Boolean,
    // Existing version
    targetVersion: Version,
    // Branch
    branch?: String,
    pullLatestChanges?: Boolean,
}

export const parsePreparedVersionToJson = (preparedVersion: PreparedVersion): object => {
    console.log(preparedVersion)
    return {
        repository: preparedVersion.repository.uri ?? null,
        submoduleReleases: preparedVersion.submoduleReleases?.map((submodule: PreparedVersion) => parsePreparedVersionToJson(submodule)) ?? [],
        updateType: preparedVersion.updateType.uri ?? null,
        versionType: preparedVersion.versionType?.uri ?? null,
        versionCode: preparedVersion.versionCode ?? null,
        releaseVersion: preparedVersion.releaseVersion ?? null,
        targetVersion: preparedVersion.targetVersion?.uri ?? null,
        branch: preparedVersion.branch ?? null,
        pullLatestChanges: preparedVersion.pullLatestChanges ?? null,
    }
}