import type Repository from "@/entities/Repository/Repository";

export interface SubmoduleListInterface {
    parentRepository: Repository,
    importableSubmodules: Array<Repository>,
    unreadableRepositories: Array<UnreadableRepositoryInterface>
}

export interface UnreadableRepositoryInterface {
    basePath: String,
    finalUrl: String,
    reason: String
}