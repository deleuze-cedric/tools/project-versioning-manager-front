import {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";

export default class Submodule extends ApiPlatformEntity {
    constructor(
        private _code: String | null = null,
        private _link: String | null = null,
        private _path: String | null = null,
        private _parent: String | null = null,
        private _child: String | null = null,
        uri: String | null = null
    ) {
        super(uri);
    }

    get code(): String | null {
        return this._code;
    }

    get link(): String | null {
        return this._link;
    }

    get path(): String | null {
        return this._path;
    }

    get parent(): String | null {
        return this._parent;
    }

    get child(): String | null {
        return this._child;
    }

    set code(value: String | null) {
        this._code = value;

        return this;
    }

    set link(value: String | null) {
        this._link = value;

        return this;
    }

    set path(value: String | null) {
        this._path = value;

        return this;
    }

    set parent(value: String | null) {
        this._parent = value;

        return this;
    }

    set child(value: String | null) {
        this._child = value;

        return this;
    }

    getIdentifier(): any {
        return this.code;
    }

    toJson(): object {
        return {
            code: this.code,
            link: this.link,
            path: this.path,
            parent: this.parent,
            child: this.child,
        };
    }
}