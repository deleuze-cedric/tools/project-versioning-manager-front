export interface UpdateType {
    label: String,
    shortLabel: String,
    id: String,
    uri: String,
}