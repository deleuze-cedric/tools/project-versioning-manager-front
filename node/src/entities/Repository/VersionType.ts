export interface VersionType {
    label: String,
    shortLabel: String,
    id: String,
    uri: String,
}