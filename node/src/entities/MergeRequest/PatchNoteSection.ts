import {swapElements} from "@/utils/Array";
import {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";

export default class PatchNoteSection extends ApiPlatformEntity {

    constructor(
        private _code?: String,
        private _name?: String,
        private _content: Array<string> = [],
        private _required: Boolean = false,
        uri: string | null = null,
    ) {
        super(uri);
    }

    get code(): String {
        return this._code ?? '';
    }

    set code(value: String) {
        this._code = value;

        return this;
    }

    get name(): String {
        return this._name ?? '';
    }

    set name(value: String) {
        this._name = value;

        return this;
    }

    get content(): Array<string> {
        return this._content;
    }

    set content(content: Array<string>) {
        this._content = content;

        return this;
    }


    public setContentAtIndex(index: Number, content: String) {
        this._content[index] = content;

        return this;
    }

    public addContent(content: String = '') {
        this._content.push(content);

        return this;
    }

    public removeContentAtIndex(index: Number) {
        this._content.splice(index, 1);

        return this;
    }

    public swapContentLines(index1: Number, index2: Number) {
        swapElements(this._content, index1, index2);
    }

    get required(): Boolean {
        return this._required;
    }

    toJson(): object {
        return {
            code: this.code,
            name: this.name,
            content: this.content,
            required: this.required,
        };
    }

    getIdentifier(): any {
        return this.code;
    }
}
