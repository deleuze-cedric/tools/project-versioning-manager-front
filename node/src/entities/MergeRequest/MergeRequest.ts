import PatchNoteSection from "@/entities/MergeRequest/PatchNoteSection";
import Repository from "@/entities/Repository/Repository";
import type MergeRequestDependencyInterface from "@/entities/MergeRequest/MergeRequestDependencyInterface";
import Version from "@/entities/Repository/Version";
import {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";

export default class MergeRequest extends ApiPlatformEntity implements MergeRequestDependencyInterface {

    private requiredSections = {
        'features': 'Fonctionnalités',
        'fix': 'Correctifs',
        'refacto': 'Refacto',
        'todo': 'TODO',
        'internal': 'Interne',
        'other': 'Autre',
    }

    constructor(
        public _code?: String,
        public _name?: String,
        public _link?: String,
        public _version: null | Version = null,
        public _sections: Array<PatchNoteSection> = [],
        public _versionDependencies: Array<Version> = [],
        public _mergeRequestDependencies: Array<MergeRequest> = [],
        public _mergedAt: null | Date = null,
        public _processedAt: null | Date = null,
        uri: string | null = null,
    ) {
        super(uri);
    }

    get code(): String {
        return this._code ?? '';
    }

    get name(): String {
        return this._name ?? '';
    }

    set name(value: String) {
        this._name = value;
    }

    get link(): String {
        return this._link ?? '';
    }

    set link(value: String) {
        this._link = value;
    }

    get repository(): null | Repository {
        return this._version?.repository ?? null;
    }

    get sections(): Array<PatchNoteSection> {
        return this._sections;
    }

    set sections(sections: Array<PatchNoteSection>) {
        this._sections = sections;

        return this;
    }

    public addSection(section: PatchNoteSection) {
        this._sections.push(section);

        return this;
    }

    public removeSectionAtIndex(index: Number) {
        this._sections.splice(index, 1);

        return this;
    }

    get versionDependencies(): Array<Version> {
        return this._versionDependencies;
    }

    set versionDependencies(dependencies: Array<Version>) {
        this._versionDependencies = dependencies;

        return this;
    }

    public addVersionDependency(dependency: Version) {
        this._versionDependencies.push(dependency);

        return this;
    }

    public removeVersionDependency(index: Number) {
        this._versionDependencies.splice(index, 1);

        return this;
    }

    get mergeRequestDependencies(): Array<MergeRequest> {
        return this._mergeRequestDependencies;
    }

    set mergeRequestDependencies(dependencies: Array<MergeRequest>) {
        this._mergeRequestDependencies = dependencies;

        return this;
    }

    public addDependency(dependency: MergeRequest) {
        this._mergeRequestDependencies.push(dependency);

        return this;
    }

    public removeDependency(index: Number) {
        this._mergeRequestDependencies.splice(index, 1);

        return this;
    }

    public bindMissingSection() {
        const foundSections: Array<String> = this.sections.map((section: PatchNoteSection) => section.code);

        for (const requiredSection in this.requiredSections) {
            if (foundSections.includes(requiredSection)) {
                continue;
            }

            this.addSection(new PatchNoteSection(requiredSection, this.requiredSections[requiredSection], [], true));
        }
    }

    public sortSections() {
        const sortedSectionsCode: Object = {...this.requiredSections};

        for (const section of this.sections) {
            const sectionCode = section.code;
            if (typeof sortedSectionsCode[sectionCode] !== 'undefined') {
                if (sortedSectionsCode[sectionCode] instanceof PatchNoteSection) {
                    sortedSectionsCode[sectionCode].content.concat(section.content);
                    continue;
                }

                sortedSectionsCode[sectionCode] = section;
            } else {
                sortedSectionsCode['other'].content = sortedSectionsCode['other'].content.concat(section.content);
            }
        }

        const newSections = [];

        for (const sectionCode in sortedSectionsCode) {
            newSections.push(sortedSectionsCode[sectionCode]);
        }

        this._sections = newSections;
    }

    public toString() {
        return this.repository?.name + ' - ' + (this.name ?? this.link)
    }

    get identifier(): any {
        return this.code;
    }

    public isMerged(): Boolean {
        return this._mergedAt !== null;
    }

    get mergedAt(): null | Date {
        return this._mergedAt;
    }

    set mergedAt(value: null | Date) {
        this._mergedAt = value;
    }

    public isProcessed(): Boolean {
        return this._processedAt !== null;
    }

    get processedAt(): null | Date {
        return this._processedAt;
    }

    set processedAt(value: null | Date) {
        this._processedAt = value;
    }

    get version(): Version | null {
        return this._version;
    }

    set version(value: Version | null) {
        this._version = value;

        return this;
    }

    toJson(): object {
        return {
            code: this.code,
            name: this.name,
            link: this.link,
            repository: this.repository?.uri,
            patchNoteSections: this.sections.map((section: PatchNoteSection) => section.toJson()),
            versionDependencies: this.versionDependencies.map((dependency: Version) => dependency.uri),
            mergeRequestDependencies: this.mergeRequestDependencies.map((dependency: MergeRequest) => dependency.uri),
            version: this.version?.uri,
            mergedAt: this.mergedAt?.toISOString()   ,
            processedAt: this.processedAt?.toISOString() ,
        };
    }

    getIdentifier(): any {
        return this.code;
    }
}
