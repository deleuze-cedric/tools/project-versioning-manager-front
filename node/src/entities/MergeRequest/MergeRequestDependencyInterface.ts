import type Repository from "@/entities/Repository/Repository";

export default interface MergeRequestDependencyInterface {

    get identifier(): any;

    get name(): String;

    get link(): String;

    get repository(): null|Repository;

}