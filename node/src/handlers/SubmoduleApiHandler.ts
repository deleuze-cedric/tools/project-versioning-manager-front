import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import Submodule from "@/entities/Repository/Submodule";

export class SubmoduleApiHandler extends ApiPlatformHandler<Submodule> {

    protected newInstance(): Submodule {
        return new Submodule();
    }

    protected getBaseEndpoint(): string {
        return '/submodules';
    }

    public mapFromApiResponse(data: object): Submodule {
        return new Submodule(
            data.code,
            data.link,
            data.path,
            data.parent,
            data.child,
            this.getUriFromData(data)
        );
    }
}