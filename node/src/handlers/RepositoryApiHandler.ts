import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import {BasedCode} from "@/entities/BasedCode/BasedCode";
import Repository from "@/entities/Repository/Repository";
import {type AxiosResponse} from "axios";
import type {SubmoduleListInterface} from "@/entities/Repository/SubmoduleListInterface";
import {MergeRequestApiHandler} from "@/handlers/MergeRequestApiHandler";
import {VersionApiHandler} from "@/handlers/VersionApiHandler";
import type {Branch} from "@/entities/Repository/Branch";
import type {PreparedVersion} from "@/entities/Repository/PreparedVersion";
import {parsePreparedVersionToJson} from "@/entities/Repository/PreparedVersion";
import {UpdateTypeApiHandler} from "@/handlers/UpdateTypeApiHandler";
import {VersionTypeApiHandler} from "@/handlers/VersionTypeApiHandler";
import {SubmoduleApiHandler} from "@/handlers/SubmoduleApiHandler";

export class RepositoryApiHandler extends ApiPlatformHandler<Repository> {

    protected newInstance(): Repository {
        return new Repository();
    }

    protected getBaseEndpoint(): string {
        return '/repositories';
    }

    public mapFromApiResponse(data: object): Repository {
        const mergeRequestHandler = new MergeRequestApiHandler();
        const versionHandler = new VersionApiHandler();
        const submoduleHandler = new SubmoduleApiHandler();

        return new Repository(
            data.code,
            data.name,
            data.link,
            data.mainBranch,
            data.devBranch,
            data.basedCodeIdentifier,
            data.token,
            data.host,
            data.createBranchForNewVersion,
            data.isAlreadyImported,
            null,
            versionHandler.mapList(data.versions),
            mergeRequestHandler.mapList(data.mergeRequests),
            submoduleHandler.mapList(data.subModules),
            submoduleHandler.mapList(data.parentRepositories),
            versionHandler.mapEntity(data.latestVersion),
            data.latestVersionCode,
            this.getUriFromData(data),
        );
    }

    public getRepositoriesFromBasedCode(basedCode: BasedCode, token: string, host: null | string, search: string | null = null): Promise<Array<Repository>> {
        let url = '/based_codes/' + basedCode.id + '/repositories?token=' + token;

        if (host) {
            url += '&host=' + host;
        }

        if (search) {
            url += '&search=' + search;
        }

        return this.executeList(url);
    }

    public prepareNewVersion(repository: Repository): Promise<PreparedVersion> {
        let url = '/repositories/' + repository.code + '/prepare_new_version';
        return this.get(url, {}, this.getBaseHeaders())
            .then((response: AxiosResponse): PreparedVersion => {
                return this.mapPrepareVersionData(response.data);
            });
    }

    private mapPrepareVersionData(data: object): PreparedVersion {
        const updateTypeRepository = new UpdateTypeApiHandler();
        const versionTypeRepository = new VersionTypeApiHandler();
        const versionRepository = new VersionApiHandler();

        return {
            repository: this.mapFromApiResponse(data.repository),
            updateType: updateTypeRepository.mapFromApiResponse(data.updateType),
            versionType: data.versionType ? versionTypeRepository.mapFromApiResponse(data.versionType) : null,
            versionCode: data.versionCode,
            releaseVersion: data.releaseVersion,
            targetVersion: data.targetVersion ? versionRepository.mapFromApiResponse(data.targetVersion) : null,
            branch: data.branch,
            pullLatestChanges: data.pullLatestChanges,
            submoduleReleases: data.submoduleReleases['hydra:member']?.map((submoduleReleaseData: object) => {
                return this.mapPrepareVersionData(submoduleReleaseData);
            })
        } as PreparedVersion;
    }

    public getSubmodules(repository: Repository): Promise<SubmoduleListInterface> {
        let url = '/based_codes/' + repository.basedCodeIdentifier + '/repositories/' + repository.code + '/submodules';
        return this.get(url, {}, this.getBaseHeaders())
            .then((response: AxiosResponse): SubmoduleListInterface => {
                const data = response.data;

                return {
                    parentRepository: this.mapFromApiResponse(data.parentRepository),
                    importableSubmodules: data.importableSubmodules['hydra:member']?.map((datum: any) => this.mapFromApiResponse(datum)) ?? [],
                    unreadableRepositories: data.unreadableRepositories['hydra:member'] ?? [],
                }
            });
    }

    public searchBranches(repository: Repository, search: String): Promise<Array<Branch>> {
        let url = '/based_codes/' + repository.basedCodeIdentifier + '/repositories/' + repository.code + '/branches?search=' + search;
        return this.get(url, {}, this.getBaseHeaders())
            .then((response: AxiosResponse): Array<Branch> => response.data['hydra:member']);
    }

    public createNewVersion(preparedVersion: PreparedVersion): Promise<AxiosResponse> {
        let url = '/repositories/' + preparedVersion.repository.code + '/new_version';

        return this.post(
            url,
            parsePreparedVersionToJson(preparedVersion),
            this.getBaseHeaders()
        );
    }
}