import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import {RepositoryApiHandler} from "@/handlers/RepositoryApiHandler";
import type Instance from "@/entities/Instance/Instance";

export class InstanceApiHandler extends ApiPlatformHandler<Instance> {

    protected newInstance(): Instance {
        return new Instance();
    }

    protected getBaseEndpoint(): string {
        return '/versions';
    }

    protected mapFromApiResponse(data: object): Instance {
        const repositoryHandler = new RepositoryApiHandler();

        return new Instance(
            data.uuid,
            data.name,
            repositoryHandler.mapEntity(data.repository),
            null,
            data.versions, // TODO VersionHistoryHandler
            this.getUriFromData(data),
        );
    }
}