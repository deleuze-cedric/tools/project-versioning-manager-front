import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import MergeRequest from "@/entities/MergeRequest/MergeRequest";
import {VersionApiHandler} from "@/handlers/VersionApiHandler";
import {PatchNoteApiHandler} from "@/handlers/PatchNoteApiHandler";

export class MergeRequestApiHandler extends ApiPlatformHandler<MergeRequest> {

    protected newInstance(): MergeRequest {
        return new MergeRequest();
    }

    protected getBaseEndpoint(): string {
        return '/merge_requests';
    }

    protected mapFromApiResponse(data: object): MergeRequest {
        const versionHandler = new VersionApiHandler();
        const patchNoteHandler = new PatchNoteApiHandler();

        return new MergeRequest(
            data.code,
            data.name,
            data.link,
            versionHandler.mapEntity(data.version),
            patchNoteHandler.mapList(data.patchNoteSections),
            versionHandler.mapList(data.versionDependencies),
            this.mapList(data.mergeRequestDependencies),
            this.mapDate(data.mergedAt),
            this.mapDate(data.processedAt),
            this.getUriFromData(data)
        );
    }
}