import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import {RepositoryApiHandler} from "@/handlers/RepositoryApiHandler";
import Version from "@/entities/Repository/Version";
import {MergeRequestApiHandler} from "@/handlers/MergeRequestApiHandler";
import {PatchNoteApiHandler} from "@/handlers/PatchNoteApiHandler";

export class VersionApiHandler extends ApiPlatformHandler<Version> {

    protected newInstance(): Version {
        return new Version();
    }

    protected getBaseEndpoint(): string {
        return '/versions';
    }

    protected mapFromApiResponse(data: object): Version {
        const repositoryHandler = new RepositoryApiHandler();
        const mergeRequestHandler = new MergeRequestApiHandler();
        const patchNoteSectionHandler = new PatchNoteApiHandler();
        return new Version(
            data.uuid,
            data.code,
            repositoryHandler.mapEntity(data.repository),
            patchNoteSectionHandler.mapList(Object.values(data.content ?? {})),
            mergeRequestHandler.mapList(data.mergeRequests),
            data.commits, // TODO : CommitApiHandler
            this.getUriFromData(data),
        );
    }
}