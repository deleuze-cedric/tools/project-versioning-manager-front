import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import type {UpdateType} from "@/entities/Repository/UpdateType";

export class UpdateTypeApiHandler extends ApiPlatformHandler<UpdateType> {
    protected newInstance(): UpdateType {
        return {} as UpdateType;
    }

    protected getBaseEndpoint(): string {
        return '/update_types';
    }

    public mapFromApiResponse(data: object): UpdateType {
        return {
            ...data,
            uri: data['@id']
        } as UpdateType;
    }
}