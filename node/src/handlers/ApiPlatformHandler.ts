import type {ApiPlatformEntity} from "@/entities/ApiPlatformEntity";
import axios, {type AxiosInstance, type AxiosResponse} from "axios";

export abstract class ApiPlatformHandler<T extends ApiPlatformEntity> {

    protected axios: AxiosInstance;

    constructor() {
        const config = {
            baseURL: this.getApiBaseUrl(),
            httpsAgent: {
                rejectUnauthorized: false
            },
            headers: this.getBaseHeaders(),
        };
        this.axios = axios.create(config);
    }

    protected getApiBaseUrl(): String {
        return import.meta.env.VITE_API_BASE_URL;
    }

    protected getBaseHeaders(contentType: string = 'application/ld+json'): object {
        return {
            'Content-Type': contentType
        };
    }

    protected async get(endpoint: string, params: object, headers: object): Promise<AxiosResponse> {
        const url = new URL(this.getApiBaseUrl() + endpoint);
        const paramsKey = Object.keys(params);

        for (const paramKey of paramsKey) {
            const value = params[paramKey];
            url.searchParams.append(paramKey.toString(), value);
        }

        const config = {
            url: url.toString(),
            headers: headers,
            method: 'get'
        }

        return this.axios.request(config);
    }

    protected async post(endpoint: string, data: object, headers: object): Promise<AxiosResponse> {
        return this.axios.request({
            url: endpoint,
            headers: headers,
            method: 'post',
            data: data
        });
    }

    protected executeGet(endpoint: string, params: object = {}, headers: object = this.getBaseHeaders()): Promise<T> {
        return this
            .get(endpoint, params, headers)
            .then((response: AxiosResponse) => {
                return this.mapFromApiResponse(response.data);
            });
    }

    protected executeList(endpoint: string, params: object = {}, headers: object = this.getBaseHeaders()): Promise<Array<T>> {
        return this
            .get(endpoint, params, headers)
            .then((response: AxiosResponse) => {
                const list = [];

                for (const data of response.data['hydra:member']) {
                    list.push(this.mapFromApiResponse(data));
                }

                return list;
            });
    }

    protected executePost(endpoint: string, entity: T, headers: object = this.getBaseHeaders()): Promise<T> {
        return this.post(
            endpoint,
            entity.toJson(),
            headers,
        ).then((response: AxiosResponse) => {
            return this.mapFromApiResponse(response.data);
        });
    }

    protected executePatch(endpoint: string, entity: T, headers: object = this.getBaseHeaders('application/merge-patch+json')): Promise<T> {
        return this.axios.request({
            url: endpoint,
            headers: headers,
            method: 'patch',
            data: entity.toJson()
        }).then((response: AxiosResponse) => {
            return this.mapFromApiResponse(response.data);
        });
    }

    public find(identifier: any): Promise<T> {
        return this.executeGet(this.getBaseEndpoint() + '/' + identifier);
    }

    public list(params: object = {}): Promise<Array<T>> {
        return this.executeList(this.getBaseEndpoint(), params);
    }

    public create(entity: T): Promise<T> {
        return this.executePost(this.getBaseEndpoint(), entity);
    }

    public update(entity: T): Promise<T> {
        return this.executePatch(this.getBaseEndpoint() + '/' + entity.getIdentifier(), entity);
    }

    public mapList(data: any): Array<T> {
        if (!data || !Array.isArray(data)) {
            return [];
        }

        return data.map((data) => this.mapEntity(data));
    }

    public mapEntity(value: any): T | null {
        if (!value) {
            return null;
        }

        if (value instanceof String) {
            const instance = this.newInstance();
            instance.uri = value;
            return instance;
        }

        return this.mapFromApiResponse(value);
    }

    public getUriFromData(data: any): String | null {
        return data['@id'] ?? null;
    }

    public mapDate(date: any): Date | null {
        if (!date) {
            return null;
        }

        return new Date(date);
    }

    protected abstract newInstance(): T;

    protected abstract getBaseEndpoint(): String;

    public abstract mapFromApiResponse(data: object): T;
}