import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import PatchNoteSection from "@/entities/MergeRequest/PatchNoteSection";

export class PatchNoteApiHandler extends ApiPlatformHandler<PatchNoteSection> {

    protected newInstance(): PatchNoteSection {
        return new PatchNoteSection();
    }

    // TODO : créer un service de deserialization au lieu de mettre dans les ApiHandler
    protected getBaseEndpoint(): string {
        return '';
    }

    protected mapFromApiResponse(data: object): PatchNoteSection {
        return new PatchNoteSection(
            data.code,
            data.name,
            data.content,
            data.required,
            this.getUriFromData(data)
        );
    }
}