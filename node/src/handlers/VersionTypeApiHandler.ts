import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import type {VersionType} from "@/entities/Repository/VersionType";

export class VersionTypeApiHandler extends ApiPlatformHandler<VersionType> {
    protected newInstance(): VersionType {
        return {} as VersionType;
    }

    protected getBaseEndpoint(): string {
        return '/version_types';
    }

    public mapFromApiResponse(data: object): VersionType {
        return {
            ...data,
            uri: data['@id']
        } as VersionType;
    }
}