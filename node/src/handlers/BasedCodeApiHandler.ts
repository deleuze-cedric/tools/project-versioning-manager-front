import {ApiPlatformHandler} from "@/handlers/ApiPlatformHandler";
import {BasedCode} from "@/entities/BasedCode/BasedCode";

export class BasedCodeApiHandler extends ApiPlatformHandler<BasedCode> {

    protected newInstance(): BasedCode {
        return new BasedCode();
    }

    protected mapFromApiResponse(data: object): BasedCode {
        return new BasedCode(
            data.id,
            data.name
        );
    }

    protected getBaseEndpoint(): String {
        return "/based_codes";
    }
}