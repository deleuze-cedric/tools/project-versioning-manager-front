import {defineStore} from 'pinia'
import {ref} from "vue";
import type {ToastMessageOptions} from "primevue/toast";
import type {AxiosResponse} from "axios";
import type {ApiPlatformViolationInterface} from "@/entities/ApiPlatformViolationInterface";
import {AxiosError} from "axios";

export const useSnackbarStore = defineStore('snackbar', () => {

    const messages: ref<Array<MessageInterface>> = ref<Array<MessageInterface>>([]);

    const createMessagesFromError = (error: Error) => {
        if(error instanceof AxiosError){
            createMessagesFromResponse(error.response);
            return;
        }

        const message: MessageInterface = {
            type: MessageType.ERROR,
            title: 'Une erreur s\'est produite',
            text: error.message
        };

        addMessage(message);
    }

    const createMessagesFromResponse = (response: AxiosResponse) => {
        const status = response.status;
        const data = response.data;
        const description = data['hydra:description'] ?? null;
        const message: MessageInterface = {
            type: MessageType.ERROR,
            title: 'Une erreur innatendue s\'est produite',
            text: description && description !== '' ? description : null,
            response: response
        };

        switch (status) {
            case 422:
                const violations = data['violations'] ?? null;

                if (!violations) {
                    message.title = 'Erreur lors de la validation de la ressource';
                    message.text ??= 'Veuillez vérifier les données envoyées';
                    break;
                }

                for (const violation: ApiPlatformViolationInterface of violations) {
                    addMessage({
                        type: MessageType.ERROR,
                        title: violation.propertyPath,
                        text: violation.message,
                        response: response
                    });
                }

                return messages;
            case 400:
                message.title = 'Votre requête est erronée';
                message.text ??= 'Veuillez réessayer plus tard, si l\'erreur persiste veuillez contacter un administrateur';
                break;
            case 401:
                message.title = 'Connexion requise';
                message.text ??= 'Vous devez être connecté pour réaliser cette action';
                break;
            case 403:
                message.title = 'Action non autorisée';
                message.text ??= 'Vous n\'avez pas les droits requis pour réaliser cette action';
                break;
            case 404:
                message.title = 'Ressource non trouvée';
                message.text ??= 'La ressource à laquelle vous essayez d\'accéder n\'existe pas';
                break;
            default:
                message.text ??= 'Veuillez réessayer plus tard, si l\'erreur persiste veuillez contacter un administrateur';
                break;
        }

        addMessage(message);
    }

    const addMessage = (message: MessageInterface, timeInMillisecond: Number = 3000) => {
        message.timeout = setTimeout(() => removeMessage(message), timeInMillisecond);
        messages.value.push(message);
    }

    const createMessage = (title: String, text: String, type: MessageType, timeInMillisecond: Number = 3000) => {
        const messageObject: MessageInterface = {title, text, type};
        addMessage(messageObject, timeInMillisecond);
    }

    const removeMessageAtIndex = (index: Number) => {
        if (index > -1) {
            const message = messages.value[index];
            const timeout = message.timeout;

            if (timeout) {
                clearTimeout(timeout);
            }

            messages.value.splice(index, 1);
        }
    }

    const removeMessage = (message: MessageInterface) => {
        const index = messages.value.findIndex((iteratedMessage: MessageInterface) => iteratedMessage.timeout === message.timeout);
        removeMessageAtIndex(index);
    }

    const removeMessageWithToast = (toast: ToastMessageOptions) => {
        const index = messages.value.findIndex((iteratedMessage: MessageInterface) => iteratedMessage.toast === toast);
        removeMessageAtIndex(index);
    }

    return {
        messages,
        createMessage,
        createMessagesFromError,
        removeMessage,
        removeMessageWithToast
    }
})

export interface MessageInterface {
    title: String,
    text: String | null,
    type: MessageType,
    timeout?: Number,
    toast?: ToastMessageOptions,
    response?: AxiosResponse
}

export enum MessageType {
    ERROR = 'error',
    WARNING = 'warn',
    SUCCESS = 'success',
    INFO = 'info',
}
