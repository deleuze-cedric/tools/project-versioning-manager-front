import {defineStore} from 'pinia'
import {ref} from "vue";
import type Repository from "@/entities/Repository/Repository";

export const useRepositoryListStore = defineStore('repositoryList', () => {

    const repositories: ref<Array<Repository>> = ref([]);

    function initRepositoryList(newRepositoryList: Array<Repository>) {
        repositories.value = newRepositoryList;
    }

    function addRepository(repository: Repository): void {
        repositories.value.push(repository);
    }

    function removeRepository(repository: Repository): void {
        let index = null;

        for (const repositoryIndex in repositories.value) {
            if (repositories.value[repositoryIndex].code === repository.code) {
                index = repositoryIndex;
                break;
            }
        }

        if (index !== null) {
            repositories.value.splice(index, 1);
        }
    }

    return {
        repositories,
        addRepository,
        removeRepository,
        initRepositoryList,
    }
})
