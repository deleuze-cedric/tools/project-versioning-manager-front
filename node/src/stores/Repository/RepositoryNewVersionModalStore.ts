import {defineStore} from 'pinia'
import {ref} from "vue";
import Repository from "@/entities/Repository/Repository";

export const useRepositoryNewVersionModalStore = defineStore('repositoryNewVersionModal', () => {

    const visible: ref<Boolean> = ref<Boolean>(false);
    const repository: ref<Repository|null> = ref<Repository|null>(null);

    function show(repositoryToVersion: Repository) {
        visible.value = true;
        repository.value = repositoryToVersion;
    }

    function hide() {
        visible.value = false;
        repository.value = null;
    }

    return {
        show,
        hide,
        visible,
        repository
    }
})
