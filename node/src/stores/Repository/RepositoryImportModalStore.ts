import {defineStore} from 'pinia'
import {ref} from "vue";
import Repository from "@/entities/Repository/Repository";
import {BasedCode} from "@/entities/BasedCode/BasedCode";

export const useRepositoryImportModalStore = defineStore('repositoryImportModal', () => {

    const visible: ref<Boolean> = ref<Boolean>(false);
    const currentView: ref<Number> = ref<Number>(0);
    const submitView: ref<Boolean> = ref<Boolean>(false);
    const showLoader: ref<Boolean> = ref<Boolean>(false);
    const repositories: ref<Array<Repository>> = ref<Array<Repository>>([]);
    const basedCode: ref<BasedCode> = ref<BasedCode>();
    const token: ref<String> = ref<String>();
    const host: ref<String | null> = ref<String | null>();
    const filterByName: ref<String | undefined> = ref<String | undefined>('');
    const importedRepository: ref<Repository> = ref<Repository>();

    function show() {
        visible.value = true;
    }

    function hide() {
        visible.value = false;
        currentView.value = 0;
        showLoader.value = false;
        submitView.value = false;
        repositories.value = [];
        basedCode.value = null;
        token.value = '';
        host.value = '';
        filterByName.value = '';
        importedRepository.value = null;
    }

    function nextView() {
        currentView.value++;
        submitView.value = false;
    }

    function previousView() {
        currentView.value--;
        submitView.value = false;
    }

    return {
        show,
        hide,
        visible,
        currentView,
        previousView,
        nextView,
        showLoader,
        submitView,
        repositories,
        basedCode,
        token,
        host,
        filterByName,
        importedRepository,
    }
})
