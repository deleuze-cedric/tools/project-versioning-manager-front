import {defineStore} from 'pinia'
import {ref} from "vue";
import type {BasedCode} from "@/entities/BasedCode/BasedCode";
import {BasedCodeApiHandler} from "@/handlers/BasedCodeApiHandler";
import {useSnackbarStore} from "@/stores/Snackbar/SnackbarStore";

export const useBasedCodeListStore = defineStore('baseCodeList', () => {

    const basedCodes: ref<Array<BasedCode>> = ref([]);
    const alreadyInSearch: ref<Boolean> = ref(false);
    const snackbarStore = useSnackbarStore();

    function init() {
        if (basedCodes.value.length > 0) {
            return;
        }

        if (alreadyInSearch.value) {
            return;
        }

        alreadyInSearch.value = true;

        const basedCodeHandler = new BasedCodeApiHandler();
        basedCodeHandler.list()
            .then((result) => basedCodes.value = result)
            .catch((error) => snackbarStore.createMessagesFromError(error))
            .finally(() => alreadyInSearch.value = false);
    }

    return {
        basedCodes,
        init,
    }
})
