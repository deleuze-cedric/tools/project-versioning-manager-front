import {defineStore} from "pinia";
import Repository from "@/entities/Repository/Repository";
import {ref} from "vue";

const LOCAL_STORAGE_REPOSITORY_ITEM = 'SELECTED_REPOSITORY';

export const usePreferencesStore = defineStore('preferences', () => {

    const selectedRepositoryFromLocalStorage: object = JSON.parse(localStorage.getItem(LOCAL_STORAGE_REPOSITORY_ITEM));
    const parsedRepo = selectedRepositoryFromLocalStorage ? Repository.fromJSON(selectedRepositoryFromLocalStorage) : null;
    const selectedRepository: ref<null | Repository> = ref<null | Repository>(parsedRepo)

    const getSelectedRepository = () => selectedRepository.value;

    const saveSelectedRepository = (repo: Repository) => {
        localStorage.setItem(LOCAL_STORAGE_REPOSITORY_ITEM, JSON.stringify(repo.toJson()));
        selectedRepository.value = repo;
    }

    return {getSelectedRepository, saveSelectedRepository}
})
