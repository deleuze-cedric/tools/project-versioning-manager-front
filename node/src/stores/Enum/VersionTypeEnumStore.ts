import {defineStore} from 'pinia'
import {ref} from "vue";
import {VersionTypeApiHandler} from "@/handlers/VersionTypeApiHandler";
import type {VersionType} from "@/entities/Repository/VersionType";

export const useVersionTypeEnumStore = defineStore('versionTypeEnum', () => {

    const versionTypes: ref<Array<VersionType>> = ref([]);

    function init() {
        if (versionTypes.value.length > 0) {
            return;
        }

        const versionTypeHandler = new VersionTypeApiHandler();
        versionTypeHandler.list().then((list: Array<VersionType>) => versionTypes.value = list);
    }

    return {
        versionTypes,
        init,
    }
})
