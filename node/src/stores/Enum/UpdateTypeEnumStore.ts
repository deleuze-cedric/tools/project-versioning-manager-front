import {defineStore} from 'pinia'
import {ref} from "vue";
import {UpdateTypeApiHandler} from "@/handlers/UpdateTypeApiHandler";
import type {UpdateType} from "@/entities/Repository/UpdateType";

export const useUpdateTypeEnumStore = defineStore('updateTypeEnum', () => {

    const updateTypes: ref<Array<UpdateType>> = ref([]);

    function init() {
        if (updateTypes.value.length > 0) {
            return;
        }

        const updateTypeHandler = new UpdateTypeApiHandler();
        updateTypeHandler.list().then((list: Array<UpdateType>) => updateTypes.value = list);
    }

    return {
        updateTypes,
        init,
    }
})
