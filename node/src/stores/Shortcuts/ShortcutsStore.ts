import {ref} from 'vue'
import {defineStore} from 'pinia'

const LOCAL_STORAGE_SHORTCUT_ITEM = 'SHOW_SHORTCUTS';

export const useShortcutsStore = defineStore('shortcut', () => {

    const showShortcutFromLocalStorage: String = localStorage.getItem(LOCAL_STORAGE_SHORTCUT_ITEM) ?? 'true';
    const visible = ref<Boolean>(showShortcutFromLocalStorage === 'true')

    function setShowShortcuts(show: Boolean) {
        visible.value = show;
        localStorage.setItem(LOCAL_STORAGE_SHORTCUT_ITEM, show.toString());
    }

    function toggle() {
        setShowShortcuts(!visible.value)
    }

    function show() {
        setShowShortcuts(true);
    }

    function hide() {
        setShowShortcuts(false);
    }

    return {visible, toggle, show, hide}
})
