import {ref} from 'vue'
import {defineStore} from 'pinia'

export const useShortcutsListenerStore = defineStore('shortcutListener', () => {

    const listeners = ref<Set<Function>>(new Set());

    function addListener(callback: Function): void {
        listeners.value.add(callback);
    }

    function removeListener(callback: Function): void {
        listeners.value.delete(callback);
    }

    function triggerKeyboardEvent(event: KeyboardEvent) {
        for (const listener of listeners.value) {
            listener(event);
        }
    }

    return {triggerKeyboardEvent, addListener, removeListener}
})
