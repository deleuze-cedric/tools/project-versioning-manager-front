import {ref} from 'vue'
import {defineStore} from 'pinia'

export const useShortcutsModalStore = defineStore('shortcutModal', () => {

    const visible = ref<Boolean>(false)

    function setShowShortcutsModal(show: Boolean) {
        visible.value = show;
    }

    function toggle() {
        setShowShortcutsModal(!visible.value)
    }

    function show() {
        setShowShortcutsModal(true);
    }

    function hide() {
        setShowShortcutsModal(false);
    }

    return {visible, toggle, show, hide}
})
