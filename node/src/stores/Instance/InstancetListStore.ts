import {defineStore} from 'pinia'
import {ref} from "vue";
import type Instance from "@/entities/Instance/Instance";

export const useInstanceListStore = defineStore('instanceList', () => {

    const instances: ref<Array<Instance>> = ref([]);

    function initInstanceList(newInstanceList: Array<Instance>) {
        instances.value = newInstanceList;
    }

    function addInstance(instance: Instance): void {
        instances.value.push(instance);
    }

    function removeInstance(instance: Instance): void {
        let index = null;

        for (const instanceIndex in instances.value) {
            if (instances.value[instanceIndex].uid === instance.uid) {
                index = instanceIndex;
                break;
            }
        }

        if (index !== null) {
            instances.value.splice(index, 1);
        }
    }

    return {
        instances,
        addInstance,
        removeInstance,
        initInstanceList,
    }
})
