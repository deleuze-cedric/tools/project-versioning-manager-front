import {defineStore} from 'pinia'
import {ref} from "vue";
import MergeRequest from "@/entities/MergeRequest/MergeRequest";

export const useMergeRequestListStore = defineStore('mergeRequestList', () => {

    const mergeRequests: ref<Array<MergeRequest>> = ref([]);
    const mergeRequestWithFocus = ref<string | null>(null);

    function initMergeRequestList(newMergeRequestList: Array<MergeRequest>) {
        mergeRequests.value = newMergeRequestList;
        focusOnFirstMergeRequest();
    }

    function addMergeRequest(mergeRequest: MergeRequest): void {
        mergeRequests.value.push(mergeRequest);

        if (mergeRequests.value.length === 1) {
            setFocusOnMergeRequest(mergeRequest.code);
        }
    }

    function removeMergeRequest(mergeRequest: MergeRequest): void {
        if (mergeRequest.code === mergeRequestWithFocus.value) {
            focusOnNextMergeRequest();
            if (!mergeRequestWithFocus.value) {
                focusOnFirstMergeRequest();
            }
        }

        let index = null;

        for (const mergeRequestIndex in mergeRequests.value) {
            if (mergeRequests.value[mergeRequestIndex].code === mergeRequest.code) {
                index = mergeRequestIndex;
                break;
            }
        }

        if (index !== null) {
            mergeRequests.value.splice(index, 1);
        }
    }

    function replaceMergeRequest(mergeRequest: MergeRequest, updatedMergeRequest: MergeRequest): void {
        let index = null;

        for (const mergeRequestIndex in mergeRequests.value) {
            if (mergeRequests.value[mergeRequestIndex].code === mergeRequest.code) {
                index = mergeRequestIndex;
                break;
            }
        }

        if (index !== null) {
            mergeRequests.value[index] = updatedMergeRequest;
        }
    }

    function setFocusOnMergeRequest(mergeRequestCode: String | null): void {
        if (null === mergeRequestCode) {
            mergeRequestWithFocus.value = null;
            return;
        }


        for (const mergeRequest of mergeRequests.value) {
            if (mergeRequest.code === mergeRequestCode) {
                mergeRequestWithFocus.value = mergeRequestCode;
                break;
            }
        }
    }

    function focusOnFirstMergeRequest(): void {
        const mergeRequestWithFocusValue = mergeRequestWithFocus.value;
        let hasMergeRequest = false;

        if (mergeRequestWithFocusValue) {
            for (const mergeRequest of mergeRequests.value) {
                if (mergeRequest.code === mergeRequestWithFocusValue) {
                    hasMergeRequest = true;
                    break;
                }
            }
        }

        if (!mergeRequestWithFocusValue || !hasMergeRequest) {
            setFocusOnMergeRequest([...mergeRequests.value][0]?.code);
        }
    }

    function focusOnNextMergeRequest(): void {
        let nextMergeRequest = null;
        let isNextMergeRequest = false;
        let mergeRequestWithFocusValue = mergeRequestWithFocus.value;

        mergeRequests.value.forEach((mergeRequest: MergeRequest) => {
            if (nextMergeRequest) {
                return;
            }

            if (isNextMergeRequest || !mergeRequestWithFocusValue) {
                nextMergeRequest = mergeRequest;
                return;
            }

            isNextMergeRequest = mergeRequest.code === mergeRequestWithFocusValue;
        })

        mergeRequestWithFocus.value = nextMergeRequest?.code;
    }

    return {
        mergeRequests,
        mergeRequestWithFocus,
        setFocusOnMergeRequest,
        addMergeRequest,
        removeMergeRequest,
        replaceMergeRequest,
        initMergeRequestList,
    }
})
