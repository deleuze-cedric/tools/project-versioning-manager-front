import ShortcutSection from "@/entities/Shortcut/ShortcutSection";
import Shortcut from "@/entities/Shortcut/Shortcut";

export enum SHORTCUTS {
    ADD_LINE = 'add-line',
    FEATURES = 'features',
    FIX = 'fix',
    HIDE_SHORTCUTS = 'hide-shortcuts',
    INTERNAL = 'internal',
    MR_DEPENDENCY = 'mr-dependency',
    MOVE_LINE_UP = 'move-line-up',
    MOVE_LINE_DOWN = 'move-line-down',
    NEW = 'new',
    NEXT_LINE = 'next-line',
    OTHER = 'other',
    PREVIOUS_LINE = 'prev-line',
    REFACTO = 'refacto',
    REMOVE_LINE = 'remove-line',
    SHOW_SHORTCUTS_MODAL = 'show-shortcuts-modal',
    TODO = 'todo',
    VALIDATE_MR = 'validate-mr',
    VERSION_DEPENDENCY = 'version-dependency',
}

export const SHORTCUTS_PER_SECTION = [
        new ShortcutSection(
            'MR/PR',
            [
                // Sections
                new Shortcut(SHORTCUTS.FEATURES, 'Fonctionnalités', ['ALT', 'SHIFT', 'F'], true, true, false, false, ['KeyF']),
                new Shortcut(SHORTCUTS.FIX, 'Correctifs', ['ALT', 'SHIFT', 'C'], true, true, false, false, ['KeyC']),
                new Shortcut(SHORTCUTS.REFACTO, 'Refacto', ['ALT', 'SHIFT', 'R'], true, true, false, false, ['KeyR']),
                new Shortcut(SHORTCUTS.TODO, 'Todo', ['ALT', 'SHIFT', 'T'], true, true, false, false, ['KeyT']),
                new Shortcut(SHORTCUTS.INTERNAL, 'Interne', ['ALT', 'SHIFT', 'I'], true, true, false, false, ['KeyI']),
                new Shortcut(SHORTCUTS.OTHER, 'Autre', ['ALT', 'SHIFT', 'A'], true, true, false, false, ['KeyQ']),
                // Contenu
                new Shortcut(SHORTCUTS.ADD_LINE, 'Nouvelle ligne de contenu', ['ALT', 'SHIFT', '+'], true, true, false, false, ['Equal', 'NumpadAdd']),
                new Shortcut(SHORTCUTS.REMOVE_LINE, 'Supprimer la ligne de contenu en cours', ['ALT', 'SHIFT', '-'], true, true, false, false, ['Digit6', 'NumpadSubtract']),
                new Shortcut(SHORTCUTS.PREVIOUS_LINE, 'Aller à la ligne de contenu précédente', ['ALT', 'SHIFT', '↑'], true, true, false, true, ['ArrowUp']),
                new Shortcut(SHORTCUTS.NEXT_LINE, 'Aller à la ligne de contenu suivante', ['ALT', 'SHIFT', '↓'], true, true, false, true, ['ArrowDown']),
                new Shortcut(SHORTCUTS.MOVE_LINE_UP, 'Déplacer la ligne vers le base', ['ALT', '↑'], true, false, false, true, ['ArrowUp']),
                new Shortcut(SHORTCUTS.MOVE_LINE_DOWN, 'Déplacer la ligne vers le haut', ['ALT', '↓'], true, false, false, true, ['ArrowDown']),
                // Dépendances
                new Shortcut(SHORTCUTS.MR_DEPENDENCY, 'Dépendances MR', ['ALT', 'SHIFT', 'M'], true, true, false, false, ['KeyM', 'Semicolon']),
                new Shortcut(SHORTCUTS.VERSION_DEPENDENCY, 'Dépendances aux versions', ['ALT', 'SHIFT', 'V'], true, true, false, false, ['KeyV']),
                // Autre
                new Shortcut(SHORTCUTS.VALIDATE_MR, 'Valider la MR', ['ALT', 'SHIFT', 'ENTREE'], true, true, false, false, ['Enter']),
            ]
        ),
        new ShortcutSection(
            'Autres',
            [
                new Shortcut(SHORTCUTS.NEW, 'Nouveau (projet, instance, ...)', ['ALT', 'SHIFT', 'N'], true, true, false, false, ['KeyN'])
            ]
        ),
        new ShortcutSection(
            'Aides',
            [
                new Shortcut(SHORTCUTS.HIDE_SHORTCUTS, 'Cacher les aides', ['ALT', 'SHIFT', 'H'], true, true, false, false, ['KeyH']),
                new Shortcut(SHORTCUTS.SHOW_SHORTCUTS_MODAL, 'Afficher le panneau d\'aide', ['ALT', 'H'], true, false, false, false, ['KeyH']),
            ]
        ),
    ]
;

export function findShortcutWithShortcutEnum(code?: SHORTCUTS): null | Shortcut {
    if (!code) {
        return null;
    }

    for (const section of SHORTCUTS_PER_SECTION) {
        for (const shortcut of section.shortcuts) {
            if (shortcut.code === code) {
                return shortcut;
            }
        }
    }

    return null;
}

export function findShortcutWithCode(code?: String): null | Shortcut {
    if (!code) {
        return null;
    }

    for (const section of SHORTCUTS_PER_SECTION) {
        for (const shortcut of section.shortcuts) {
            if (shortcut.code.toString() === code) {
                return shortcut;
            }
        }
    }

    return null;
}
